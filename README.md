# WebEOS CGI Testing

This project contains a set of files that are used for the okd4-install tests. These files are copied to the following directory in EOS: `/eos/user/o/okdtests/webeos-tests/`. A Gitlab CI pipeline is used to automatically copy the files on EOS.


To test webeos properly we need to:

1. Clone this repo to our `EOS` home directory (we can do this using lxplus, aiadm or cernbox desktop app);

2. We head over to [cernbox.cern.ch](cernbox.cern.ch) and now we will have to share the repo folder with the service account `a:wwweos` (for prod testing) `a:eosautofstests` (for dev testing)

3. Make sure that every thing under the folder `cgi-bin` is executable you can do this by running `chmod +x hello.*`

4. Now we only need to create a `UserProvidedDirectory` CR with `path` pointing to the repo folder we have created.
```
apiVersion: webeos.webservices.cern.ch/v1alpha1
kind: UserProvidedDirectory
metadata:
  name: <NAME>
spec:
  ...
  eosPath: /eos/user/<CERN_ID_INITIAL>/<CERN_ID>/webeos-cgi-test
```

5. To test that everything is working properly we can just access the website followed by `/cgi-bin/hello.pl` a `Hello, World!` message should appear in the browser.

Note: As of July 2020 PHP, Perl, Bash and Python work.