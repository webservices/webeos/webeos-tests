# Create squashfs package with the contents of a EOS directory in an EOS path 

# Input:
# $1: The EOS directory of the package
# $2: The subdirectory to copy into the package

echo "Input params: "
echo "Package path: $1"
echo "Copied Directory: $2"

eos root://eoshome.cern.ch squash rm $1
eos root://eoshome.cern.ch squash new $1
cp -r $2/. $1/
eos root://eoshome.cern.ch squash pack $1
